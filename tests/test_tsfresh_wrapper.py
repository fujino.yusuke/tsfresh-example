import pytest
import pandas as pd
from pandas.testing import assert_frame_equal
import numpy as np
from tsfresh_wrapper import (
    __version__,
    make_timeseries_modeling_dataset,
    DEFAULT_FORECAST_DISTANCE_COL,
)


@pytest.fixture
def df_multi_ts() -> pd.DataFrame:
    return pd.DataFrame(
        {
            "ID": [*np.repeat("foo", 5), *np.repeat("bar", 5)],
            "t": [f"2021/01/{day:02}" for day in range(1, 11)],
            "v": np.arange(10).astype(np.float64),
            "is_holiday": [True, True, True, *np.repeat(False, 5), True, True],
            "is_sale": [False, True, *np.repeat(False, 6), True, False],
        }
    )


@pytest.fixture
def df_multi_ts_pred(df_multi_ts: pd.DataFrame) -> pd.DataFrame:
    df_ret = df_multi_ts.copy()
    df_ret["t"] = [
        *[f"2021/01/{day:02}" for day in range(1, 6)],
        *[f"2021/01/{day:02}" for day in range(1, 6)],
    ]
    df_ret["v"] = [
        *np.arange(4).astype(np.float64),
        None,
        *np.arange(5, 9).astype(np.float64),
        None,
    ]
    return df_ret


def test_version() -> None:
    assert __version__ == "0.2.0"


index_types = [int, str]
gaps = [0, 1]


class TestMakeTimeSeriesModelingDataset(object):
    @pytest.mark.parametrize("index_type", index_types)
    @pytest.mark.parametrize("gap", gaps)
    def test_multi_series(
        self, index_type: type, gap: int, df_multi_ts: pd.DataFrame
    ) -> None:
        df_multi_ts.index = df_multi_ts.index.astype(index_type)
        df_actual = make_timeseries_modeling_dataset(
            df_multi_ts,
            "t",
            "v",
            -1 - gap,
            -gap,
            1,
            1,
            group_col="ID",
            known_in_advance_features=["is_holiday", "is_sale"],
            default_fc_parameters={"mean": None},
        )
        df_expected = (
            pd.DataFrame(
                {
                    "ID": df_multi_ts["ID"],
                    "t": df_multi_ts["t"],
                    "v": [*np.arange(1, 5), np.nan, *np.arange(6, 10), np.nan],
                    DEFAULT_FORECAST_DISTANCE_COL: np.ones(len(df_multi_ts)).astype(
                        np.int64
                    ),
                    "__TSFRESH_WRAPPER__t__AT_FORECAST_DISTANCE__": [
                        *[f"2021/01/{day:02}" for day in range(2, 6)],
                        None,
                        *[f"2021/01/{day:02}" for day in range(7, 11)],
                        None,
                    ],
                    "is_holiday": [
                        *np.repeat(True, 2),
                        *np.repeat(False, 2),
                        np.nan,
                        *np.repeat(False, 2),
                        *np.repeat(True, 2),
                        np.nan,
                    ],
                    "is_sale": [
                        True,
                        *np.repeat(False, 3),
                        np.nan,
                        *np.repeat(False, 2),
                        True,
                        False,
                        np.nan,
                    ],
                    "v__mean": [0.0, 0.5, 1.5, 2.5, 3.5, 5.0, 5.5, 6.5, 7.5, 8.5]
                    if gap == 0
                    else [np.nan, 0.0, 0.5, 1.5, 2.5, np.nan, 5.0, 5.5, 6.5, 7.5],
                }
            )
            .sort_values(by=["ID", "t"])
            .reset_index(drop=True)
        )
        assert_frame_equal(df_actual, df_expected)

    @pytest.mark.parametrize("index_type", index_types)
    @pytest.mark.parametrize("gap", gaps)
    def test_single_series(
        self, index_type: type, gap: int, df_multi_ts: pd.DataFrame
    ) -> None:
        df_single_ts = df_multi_ts[df_multi_ts["ID"] == "foo"].drop(columns="ID")
        df_single_ts.index = df_single_ts.index.astype(index_type)
        df_actual = make_timeseries_modeling_dataset(
            df_single_ts,
            "t",
            "v",
            -1 - gap,
            -gap,
            1,
            1,
            known_in_advance_features=["is_holiday", "is_sale"],
            default_fc_parameters={"mean": None},
        )
        df_expected = (
            pd.DataFrame(
                {
                    "t": df_single_ts["t"],
                    "v": [*np.arange(1, 5), np.nan],
                    DEFAULT_FORECAST_DISTANCE_COL: np.ones(5).astype(np.int64),
                    "__TSFRESH_WRAPPER__t__AT_FORECAST_DISTANCE__": [
                        *[f"2021/01/{day:02}" for day in range(2, 6)],
                        None,
                    ],
                    "is_holiday": [*np.repeat(True, 2), *np.repeat(False, 2), np.nan],
                    "is_sale": [True, *np.repeat(False, 3), np.nan],
                    "v__mean": [0.0, 0.5, 1.5, 2.5, 3.5]
                    if gap == 0
                    else [np.nan, 0.0, 0.5, 1.5, 2.5],
                }
            )
            .sort_values(by="t")
            .reset_index(drop=True)
        )
        assert_frame_equal(df_actual, df_expected)

    @pytest.mark.parametrize("index_type", index_types)
    @pytest.mark.parametrize("gap", gaps)
    def test_multi_series_multi_forecast_distance(
        self, index_type: type, gap: int, df_multi_ts: pd.DataFrame
    ) -> None:
        df_multi_ts.index = df_multi_ts.index.astype(index_type)
        df_actual = make_timeseries_modeling_dataset(
            df_multi_ts,
            "t",
            "v",
            -1 - gap,
            -gap,
            1,
            2,
            group_col="ID",
            known_in_advance_features=["is_holiday", "is_sale"],
            default_fc_parameters={"mean": None},
        )

        def _expected(fd: int) -> pd.DataFrame:
            return pd.DataFrame(
                {
                    "ID": df_multi_ts["ID"],
                    "t": df_multi_ts["t"],
                    "v": [
                        *np.arange(fd, 5),
                        *np.repeat(np.nan, fd),
                        *np.arange(5 + fd, 10),
                        *np.repeat(np.nan, fd),
                    ],
                    DEFAULT_FORECAST_DISTANCE_COL: np.repeat(fd, len(df_multi_ts)),
                    "__TSFRESH_WRAPPER__t__AT_FORECAST_DISTANCE__": [
                        *[f"2021/01/{day:02}" for day in range(1 + fd, 6)],
                        *np.repeat(np.nan, fd),
                        *[f"2021/01/{day:02}" for day in range(6 + fd, 11)],
                        *np.repeat(np.nan, fd),
                    ],
                    "is_holiday": [
                        *np.repeat(True, 2),
                        *np.repeat(False, 2),
                        np.nan,
                        *np.repeat(False, 2),
                        *np.repeat(True, 2),
                        np.nan,
                    ]
                    if fd == 1
                    else [
                        True,
                        *np.repeat(False, 2),
                        *np.repeat(np.nan, 2),
                        *np.repeat(False, 1),
                        *np.repeat(True, 2),
                        *np.repeat(np.nan, 2),
                    ],
                    "is_sale": [
                        True,
                        *np.repeat(False, 3),
                        np.nan,
                        *np.repeat(False, 2),
                        True,
                        False,
                        np.nan,
                    ]
                    if fd == 1
                    else [
                        *np.repeat(False, 3),
                        *np.repeat(np.nan, 2),
                        False,
                        True,
                        False,
                        *np.repeat(np.nan, 2),
                    ],
                    "v__mean": [0.0, 0.5, 1.5, 2.5, 3.5, 5.0, 5.5, 6.5, 7.5, 8.5]
                    if gap == 0
                    else [np.nan, 0.0, 0.5, 1.5, 2.5, np.nan, 5.0, 5.5, 6.5, 7.5],
                }
            )

        df_expected = (
            pd.concat([_expected(1), _expected(2)])
            .sort_values(by=["ID", DEFAULT_FORECAST_DISTANCE_COL, "t"])
            .reset_index(drop=True)
        )
        assert_frame_equal(df_actual, df_expected)

    @pytest.mark.parametrize("index_type", index_types)
    @pytest.mark.parametrize("gap", gaps)
    def test_single_series_multi_forecast_distance(
        self, index_type: type, gap: int, df_multi_ts: pd.DataFrame
    ) -> None:
        df_single_ts = df_multi_ts[df_multi_ts["ID"] == "foo"].drop(columns="ID")
        df_single_ts.index = df_single_ts.index.astype(index_type)
        df_actual = make_timeseries_modeling_dataset(
            df_single_ts,
            "t",
            "v",
            -1 - gap,
            -gap,
            1,
            2,
            known_in_advance_features=["is_holiday", "is_sale"],
            default_fc_parameters={"mean": None},
        )

        def _expected(fd: int) -> pd.DataFrame:
            return pd.DataFrame(
                {
                    "t": df_single_ts["t"],
                    "v": [*np.arange(fd, 5), *np.repeat(np.nan, fd)],
                    DEFAULT_FORECAST_DISTANCE_COL: np.repeat(fd, len(df_single_ts)),
                    "__TSFRESH_WRAPPER__t__AT_FORECAST_DISTANCE__": [
                        *[f"2021/01/{day:02}" for day in range(1 + fd, 6)],
                        *np.repeat(np.nan, fd),
                    ],
                    "is_holiday": [*np.repeat(True, 2), *np.repeat(False, 2), np.nan]
                    if fd == 1
                    else [True, *np.repeat(False, 2), *np.repeat(np.nan, 2)],
                    "is_sale": [True, *np.repeat(False, 3), np.nan]
                    if fd == 1
                    else [*np.repeat(False, 3), *np.repeat(np.nan, 2)],
                    "v__mean": [0.0, 0.5, 1.5, 2.5, 3.5]
                    if gap == 0
                    else [np.nan, 0.0, 0.5, 1.5, 2.5],
                }
            )

        df_expected = (
            pd.concat([_expected(1), _expected(2)])
            .sort_values(by=[DEFAULT_FORECAST_DISTANCE_COL, "t"])
            .reset_index(drop=True)
        )
        assert_frame_equal(df_actual, df_expected)

    @pytest.mark.parametrize("index_type", index_types)
    @pytest.mark.parametrize("gap", gaps)
    def test_single_series_for_prediction(
        self, index_type: type, gap: int, df_multi_ts: pd.DataFrame
    ) -> None:
        df_single_ts = df_multi_ts[df_multi_ts["ID"] == "foo"].drop(columns="ID")
        df_single_ts.index = df_single_ts.index.astype(index_type)

        # Replace last element of "v" to missing value
        df_single_ts["v"] = [*df_single_ts["v"].iloc[:-1].values, None]

        df_actual = make_timeseries_modeling_dataset(
            df_single_ts,
            "t",
            "v",
            -1 - gap,
            -gap,
            1,
            1,
            known_in_advance_features=["is_holiday", "is_sale"],
            prediction_point=df_single_ts["t"].iloc[-2],
            default_fc_parameters={"mean": None},
        )
        df_expected = (
            pd.DataFrame(
                {
                    "t": ["2021/01/04"],
                    "v": [np.nan],
                    DEFAULT_FORECAST_DISTANCE_COL: [1],
                    "__TSFRESH_WRAPPER__t__AT_FORECAST_DISTANCE__": ["2021/01/05"],
                    "is_holiday": [False],
                    "is_sale": [False],
                    "v__mean": [2.5] if gap == 0 else [1.5],
                }
            )
            .sort_values(by="t")
            .reset_index(drop=True)
        )
        assert_frame_equal(df_actual, df_expected)

    @pytest.mark.parametrize("index_type", index_types)
    @pytest.mark.parametrize("gap", gaps)
    def test_multi_series_for_prediction(
        self, index_type: type, gap: int, df_multi_ts_pred: pd.DataFrame
    ) -> None:
        df_multi_ts_pred.index = df_multi_ts_pred.index.astype(index_type)
        df_actual = make_timeseries_modeling_dataset(
            df_multi_ts_pred,
            "t",
            "v",
            -1 - gap,
            -gap,
            1,
            1,
            group_col="ID",
            known_in_advance_features=["is_holiday", "is_sale"],
            prediction_point="2021/01/04",
            default_fc_parameters={"mean": None},
        )
        df_expected = (
            pd.DataFrame(
                {
                    "ID": ["foo", "bar"],
                    "t": ["2021/01/04", "2021/01/04"],
                    "v": [np.nan, np.nan],
                    DEFAULT_FORECAST_DISTANCE_COL: np.ones(2).astype(np.int64),
                    "__TSFRESH_WRAPPER__t__AT_FORECAST_DISTANCE__": [
                        "2021/01/05",
                        "2021/01/05",
                    ],
                    "is_holiday": [False, True],
                    "is_sale": [False, False],
                    "v__mean": [2.5, 7.5] if gap == 0 else [1.5, 6.5],
                }
            )
            .sort_values(by=["ID", "t"])
            .reset_index(drop=True)
        )
        assert_frame_equal(df_actual, df_expected)

    @pytest.mark.parametrize("index_type", index_types)
    @pytest.mark.parametrize("gap", gaps)
    def test_single_series_for_prediction_multi_forecast_distance(
        self, index_type: type, gap: int, df_multi_ts: pd.DataFrame
    ) -> None:
        df_single_ts = df_multi_ts[df_multi_ts["ID"] == "foo"].drop(columns="ID")
        df_single_ts.index = df_single_ts.index.astype(index_type)

        # Replace last two element of "v" to missing value
        df_single_ts["v"] = [*df_single_ts["v"].iloc[:-2].values, None, None]

        df_actual = make_timeseries_modeling_dataset(
            df_single_ts,
            "t",
            "v",
            -1 - gap,
            -gap,
            1,
            2,
            known_in_advance_features=["is_holiday", "is_sale"],
            prediction_point=df_single_ts["t"].iloc[-3],
            default_fc_parameters={"mean": None},
        )

        def _expected(fd: int) -> pd.DataFrame:
            return pd.DataFrame(
                {
                    "t": ["2021/01/03"],
                    "v": [np.nan],
                    DEFAULT_FORECAST_DISTANCE_COL: [fd],
                    "__TSFRESH_WRAPPER__t__AT_FORECAST_DISTANCE__": [
                        f"2021/01/{3 + fd:02}"
                    ],
                    "is_holiday": [False] if fd == 1 else [False],
                    "is_sale": [False] if fd == 1 else [False],
                    "v__mean": [1.5] if gap == 0 else [0.5],
                }
            )

        df_expected = (
            pd.concat([_expected(1), _expected(2)])
            .sort_values(by=[DEFAULT_FORECAST_DISTANCE_COL, "t"])
            .reset_index(drop=True)
        )
        assert_frame_equal(df_actual, df_expected)

    @pytest.mark.parametrize("index_type", index_types)
    @pytest.mark.parametrize("gap", gaps)
    def test_multi_series_for_prediction_multiple_forecast_distance(
        self, index_type: type, gap: int, df_multi_ts_pred: pd.DataFrame
    ) -> None:
        df_multi_ts_pred.index = df_multi_ts_pred.index.astype(index_type)
        df_multi_ts_pred["v"] = [
            *np.arange(3).astype(np.float64),
            None,
            None,
            *np.arange(5, 8).astype(np.float64),
            None,
            None,
        ]
        df_actual = make_timeseries_modeling_dataset(
            df_multi_ts_pred,
            "t",
            "v",
            -1 - gap,
            -gap,
            1,
            2,
            group_col="ID",
            known_in_advance_features=["is_holiday", "is_sale"],
            prediction_point="2021/01/03",
            default_fc_parameters={"mean": None},
        )

        def _expected(fd: int) -> pd.DataFrame:
            return pd.DataFrame(
                {
                    "ID": ["foo", "bar"],
                    "t": ["2021/01/03", "2021/01/03"],
                    "v": [np.nan, np.nan],
                    DEFAULT_FORECAST_DISTANCE_COL: np.repeat(fd, 2),
                    "__TSFRESH_WRAPPER__t__AT_FORECAST_DISTANCE__": [
                        f"2021/01/{3 + fd:02}",
                        f"2021/01/{3 + fd:02}",
                    ],
                    "is_holiday": [False, True] if fd == 1 else [False, True],
                    "is_sale": [False, True] if fd == 1 else [False, False],
                    "v__mean": [1.5, 6.5] if gap == 0 else [0.5, 5.5],
                }
            )

        df_expected = (
            pd.concat([_expected(1), _expected(2)])
            .sort_values(by=["ID", DEFAULT_FORECAST_DISTANCE_COL, "t"])
            .reset_index(drop=True)
        )
        assert_frame_equal(df_actual, df_expected)

    @pytest.mark.parametrize("index_type", index_types)
    @pytest.mark.parametrize("gap", gaps)
    def test_multi_series_ignore_derivation(
        self, index_type: type, gap: int, df_multi_ts: pd.DataFrame
    ) -> None:
        df_multi_ts.index = df_multi_ts.index.astype(index_type)
        df_multi_ts["v2"] = np.arange(10, 10 + len(df_multi_ts))
        df_multi_ts["v3"] = np.arange(20, 20 + len(df_multi_ts))
        df_actual = make_timeseries_modeling_dataset(
            df_multi_ts,
            "t",
            "v",
            -1 - gap,
            -gap,
            1,
            1,
            group_col="ID",
            known_in_advance_features=["is_holiday", "is_sale"],
            ignore_derivation=["v2", "v3"],
            default_fc_parameters={"mean": None},
        )
        df_expected = (
            pd.DataFrame(
                {
                    "ID": df_multi_ts["ID"],
                    "t": df_multi_ts["t"],
                    "v": [*np.arange(1, 5), np.nan, *np.arange(6, 10), np.nan],
                    DEFAULT_FORECAST_DISTANCE_COL: np.ones(len(df_multi_ts)).astype(
                        np.int64
                    ),
                    "__TSFRESH_WRAPPER__t__AT_FORECAST_DISTANCE__": [
                        *[f"2021/01/{day:02}" for day in range(2, 6)],
                        None,
                        *[f"2021/01/{day:02}" for day in range(7, 11)],
                        None,
                    ],
                    "is_holiday": [
                        *np.repeat(True, 2),
                        *np.repeat(False, 2),
                        np.nan,
                        *np.repeat(False, 2),
                        *np.repeat(True, 2),
                        np.nan,
                    ],
                    "is_sale": [
                        True,
                        *np.repeat(False, 3),
                        np.nan,
                        *np.repeat(False, 2),
                        True,
                        False,
                        np.nan,
                    ],
                    "__TSFRESH_WRAPPER__v2__NO_DERIVATION__": df_multi_ts["v2"],
                    "__TSFRESH_WRAPPER__v3__NO_DERIVATION__": df_multi_ts["v3"],
                    "v__mean": [0.0, 0.5, 1.5, 2.5, 3.5, 5.0, 5.5, 6.5, 7.5, 8.5]
                    if gap == 0
                    else [np.nan, 0.0, 0.5, 1.5, 2.5, np.nan, 5.0, 5.5, 6.5, 7.5],
                }
            )
            .sort_values(by=["ID", "t"])
            .reset_index(drop=True)
        )
        assert_frame_equal(df_actual, df_expected)

    @pytest.mark.parametrize("index_type", index_types)
    @pytest.mark.parametrize("gap", gaps)
    def test_single_series_ignore_derivation(
        self, index_type: type, gap: int, df_multi_ts: pd.DataFrame
    ) -> None:
        df_single_ts = df_multi_ts[df_multi_ts["ID"] == "foo"].drop(columns="ID")
        df_single_ts.index = df_single_ts.index.astype(index_type)
        df_single_ts["v2"] = np.arange(10, 10 + len(df_single_ts))
        df_single_ts["v3"] = np.arange(20, 20 + len(df_single_ts))
        df_actual = make_timeseries_modeling_dataset(
            df_single_ts,
            "t",
            "v",
            -1 - gap,
            -gap,
            1,
            1,
            known_in_advance_features=["is_holiday", "is_sale"],
            ignore_derivation=["v2", "v3"],
            default_fc_parameters={"mean": None},
        )
        df_expected = (
            pd.DataFrame(
                {
                    "t": df_single_ts["t"],
                    "v": [*np.arange(1, 5), np.nan],
                    DEFAULT_FORECAST_DISTANCE_COL: np.ones(5).astype(np.int64),
                    "__TSFRESH_WRAPPER__t__AT_FORECAST_DISTANCE__": [
                        *[f"2021/01/{day:02}" for day in range(2, 6)],
                        None,
                    ],
                    "is_holiday": [*np.repeat(True, 2), *np.repeat(False, 2), np.nan],
                    "is_sale": [True, *np.repeat(False, 3), np.nan],
                    "__TSFRESH_WRAPPER__v2__NO_DERIVATION__": df_single_ts["v2"],
                    "__TSFRESH_WRAPPER__v3__NO_DERIVATION__": df_single_ts["v3"],
                    "v__mean": [0.0, 0.5, 1.5, 2.5, 3.5]
                    if gap == 0
                    else [np.nan, 0.0, 0.5, 1.5, 2.5],
                }
            )
            .sort_values(by="t")
            .reset_index(drop=True)
        )
        assert_frame_equal(df_actual, df_expected)

    @pytest.mark.parametrize("index_type", index_types)
    @pytest.mark.parametrize("gap", gaps)
    def test_multi_series_ignore_all_feature_derivation(
        self, index_type: type, gap: int, df_multi_ts: pd.DataFrame
    ) -> None:
        df_multi_ts.index = df_multi_ts.index.astype(index_type)
        df_multi_ts["v2"] = np.arange(10, 10 + len(df_multi_ts))
        df_multi_ts["v3"] = np.arange(20, 20 + len(df_multi_ts))
        df_actual = make_timeseries_modeling_dataset(
            df_multi_ts,
            "t",
            "v",
            -1 - gap,
            -gap,
            1,
            1,
            group_col="ID",
            known_in_advance_features=["is_holiday", "is_sale"],
            ignore_derivation=[col for col in df_multi_ts if col not in ["ID", "t"]],
            default_fc_parameters={"mean": None},
        )
        df_expected = (
            pd.DataFrame(
                {
                    "ID": df_multi_ts["ID"],
                    "t": df_multi_ts["t"],
                    "v": [*np.arange(1, 5), np.nan, *np.arange(6, 10), np.nan],
                    DEFAULT_FORECAST_DISTANCE_COL: np.ones(len(df_multi_ts)).astype(
                        np.int64
                    ),
                    "__TSFRESH_WRAPPER__t__AT_FORECAST_DISTANCE__": [
                        *[f"2021/01/{day:02}" for day in range(2, 6)],
                        None,
                        *[f"2021/01/{day:02}" for day in range(7, 11)],
                        None,
                    ],
                    "is_holiday": [
                        *np.repeat(True, 2),
                        *np.repeat(False, 2),
                        np.nan,
                        *np.repeat(False, 2),
                        *np.repeat(True, 2),
                        np.nan,
                    ],
                    "is_sale": [
                        True,
                        *np.repeat(False, 3),
                        np.nan,
                        *np.repeat(False, 2),
                        True,
                        False,
                        np.nan,
                    ],
                    "__TSFRESH_WRAPPER__v__NO_DERIVATION__": df_multi_ts["v"],
                    "__TSFRESH_WRAPPER__v2__NO_DERIVATION__": df_multi_ts["v2"],
                    "__TSFRESH_WRAPPER__v3__NO_DERIVATION__": df_multi_ts["v3"],
                }
            )
            .sort_values(by=["ID", "t"])
            .reset_index(drop=True)
        )
        assert all([(col in df_actual.columns) for col in df_expected.columns])
        assert_frame_equal(df_actual, df_expected)

    @pytest.mark.parametrize("index_type", index_types)
    @pytest.mark.parametrize("gap", gaps)
    def test_single_series_ignore_all_feature_derivation(
        self, index_type: type, gap: int, df_multi_ts: pd.DataFrame
    ) -> None:
        df_single_ts = df_multi_ts[df_multi_ts["ID"] == "foo"].drop(columns="ID")
        df_single_ts.index = df_single_ts.index.astype(index_type)
        df_single_ts["v2"] = np.arange(10, 10 + len(df_single_ts))
        df_single_ts["v3"] = np.arange(20, 20 + len(df_single_ts))
        df_actual = make_timeseries_modeling_dataset(
            df_single_ts,
            "t",
            "v",
            -1 - gap,
            -gap,
            1,
            1,
            known_in_advance_features=["is_holiday", "is_sale"],
            ignore_derivation=[col for col in df_single_ts if col not in ["ID", "t"]],
            default_fc_parameters={"mean": None},
        )
        df_expected = (
            pd.DataFrame(
                {
                    "t": df_single_ts["t"],
                    "v": [*np.arange(1, 5), np.nan],
                    DEFAULT_FORECAST_DISTANCE_COL: np.ones(5).astype(np.int64),
                    "__TSFRESH_WRAPPER__t__AT_FORECAST_DISTANCE__": [
                        *[f"2021/01/{day:02}" for day in range(2, 6)],
                        None,
                    ],
                    "is_holiday": [*np.repeat(True, 2), *np.repeat(False, 2), np.nan],
                    "is_sale": [True, *np.repeat(False, 3), np.nan],
                    "__TSFRESH_WRAPPER__v__NO_DERIVATION__": df_single_ts["v"],
                    "__TSFRESH_WRAPPER__v2__NO_DERIVATION__": df_single_ts["v2"],
                    "__TSFRESH_WRAPPER__v3__NO_DERIVATION__": df_single_ts["v3"],
                }
            )
            .sort_values(by="t")
            .reset_index(drop=True)
        )
        assert all([(col in df_actual.columns) for col in df_expected.columns])
        assert_frame_equal(df_actual, df_expected)

    @pytest.mark.parametrize("index_type", index_types)
    @pytest.mark.parametrize("gap", gaps)
    def test_multi_series_kind_to_fc(
        self, index_type: type, gap: int, df_multi_ts: pd.DataFrame
    ) -> None:
        df_multi_ts.index = df_multi_ts.index.astype(index_type)
        df_multi_ts["v2"] = [1.0, -2.0, 3.0, -4.0, 5.0, -6.0, 7.0, -8.0, 9.0, -10.0]
        df_multi_ts["v3"] = [1.1, -2.1, 3.1, -4.1, 5.1, -6.1, 7.1, -8.1, 9.1, -10.1]
        df_actual = make_timeseries_modeling_dataset(
            df_multi_ts,
            "t",
            "v",
            -1 - gap,
            -gap,
            1,
            1,
            group_col="ID",
            known_in_advance_features=["is_holiday", "is_sale"],
            default_fc_parameters={"mean": None},
            kind_to_fc_parameters={"v2": {"maximum": None}, "v3": {"minimum": None}},
        )
        df_expected = (
            pd.DataFrame(
                {
                    "ID": df_multi_ts["ID"],
                    "t": df_multi_ts["t"],
                    "v": [*np.arange(1, 5), np.nan, *np.arange(6, 10), np.nan],
                    DEFAULT_FORECAST_DISTANCE_COL: np.ones(len(df_multi_ts)).astype(
                        np.int64
                    ),
                    "__TSFRESH_WRAPPER__t__AT_FORECAST_DISTANCE__": [
                        *[f"2021/01/{day:02}" for day in range(2, 6)],
                        None,
                        *[f"2021/01/{day:02}" for day in range(7, 11)],
                        None,
                    ],
                    "is_holiday": [
                        *np.repeat(True, 2),
                        *np.repeat(False, 2),
                        np.nan,
                        *np.repeat(False, 2),
                        *np.repeat(True, 2),
                        np.nan,
                    ],
                    "is_sale": [
                        True,
                        *np.repeat(False, 3),
                        np.nan,
                        *np.repeat(False, 2),
                        True,
                        False,
                        np.nan,
                    ],
                    "v3__minimum": [
                        1.1,
                        -2.1,
                        -2.1,
                        -4.1,
                        -4.1,
                        -6.1,
                        -6.1,
                        -8.1,
                        -8.1,
                        -10.1,
                    ]
                    if gap == 0
                    else [
                        np.nan,
                        1.1,
                        -2.1,
                        -2.1,
                        -4.1,
                        np.nan,
                        -6.1,
                        -6.1,
                        -8.1,
                        -8.1,
                    ],
                    "v2__maximum": [1.0, 1.0, 3.0, 3.0, 5.0, -6.0, 7.0, 7.0, 9.0, 9.0]
                    if gap == 0
                    else [np.nan, 1.0, 1.0, 3.0, 3.0, np.nan, -6.0, 7.0, 7.0, 9.0],
                    "v__mean": [0.0, 0.5, 1.5, 2.5, 3.5, 5.0, 5.5, 6.5, 7.5, 8.5]
                    if gap == 0
                    else [np.nan, 0.0, 0.5, 1.5, 2.5, np.nan, 5.0, 5.5, 6.5, 7.5],
                }
            )
            .sort_values(by=["ID", "t"])
            .reset_index(drop=True)
        )
        assert df_actual.shape == df_expected.shape
        assert all([(col in df_actual.columns) for col in df_expected.columns])
        assert_frame_equal(df_actual[df_expected.columns], df_expected)

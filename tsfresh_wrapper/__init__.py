import sys
from argparse import ArgumentParser, Namespace
from typing import List, Dict, Any, Optional, Callable
import pandas as pd
import numpy as np
from tsfresh import extract_features
from tsfresh.utilities.dataframe_functions import roll_time_series
from tsfresh.feature_extraction import (
    ComprehensiveFCParameters,
    MinimalFCParameters,
    EfficientFCParameters,
)
from logging import getLogger, Logger


__version__ = "0.2.0"


DEFAULT_BASE_NAME = "TSFRESH_WRAPPER"
DEFAULT_GROUP_COL_NAME = f"__{DEFAULT_BASE_NAME}_GROUP_ID__"
DEFAULT_FORECAST_DISTANCE_COL = f"__{DEFAULT_BASE_NAME}_FORECAST_DISTANCE__"
DEFAULT_LOGGER = getLogger()
FEATURE_CALC_TYPES = {
    "MINIMAL": MinimalFCParameters(),
    "EFFICIENT": EfficientFCParameters(),
    "COMPREHENSIVE": ComprehensiveFCParameters(),
}
DEFAULT_FEATURE_CALC_TYPE = "MINIMAL"


def to_forecast_col_name(target_col: str) -> str:
    return f"__{DEFAULT_BASE_NAME}__{target_col}__AT_FORECAST_DISTANCE__"


def to_no_derivation_col_name(col: str) -> str:
    return f"__{DEFAULT_BASE_NAME}__{col}__NO_DERIVATION__"


def add_dummy_group_id(df: pd.DataFrame) -> pd.DataFrame:
    return pd.concat(
        [pd.Series(np.ones(len(df)), name=DEFAULT_GROUP_COL_NAME, index=df.index), df],
        axis=1,
    )


def derive_features_for_multi_series(
    df: pd.DataFrame,
    time_col: str,
    group_col: str,
    derivation_window_from: int,
    derivation_window_to: int,
    *,
    derivation_window_min_size: int = 0,
    default_fc_parameters: Dict[
        str, Optional[List[Dict[str, Any]]]
    ] = FEATURE_CALC_TYPES[DEFAULT_FEATURE_CALC_TYPE],
    kind_to_fc_parameters: Dict[str, Dict[str, Optional[List[Dict[str, Any]]]]] = {},
    logger: Logger = DEFAULT_LOGGER,
) -> pd.DataFrame:
    """
    Derive features for multi time series dataset

    Parameters
    ----------
    df: pd.DataFrame
        Input DataFrame
    time_col: str
        Time column name
    group_col: str
        Group ID column name.
    derivation_window_from: int
        Start point of the derivation window. This must be negative value.
    derivation_window_to: int
        End point of the derivation window. This must be negative value.

    Optional Parameters
    -------------------
    derivation_window_min_size: int
        Minimum length of derivation window.
    default_fc_parameters: Dict[str, Optional[List[Dict[str, Any]]]]
        Feature extraction setting.
        See https://tsfresh.readthedocs.io/en/latest/text/feature_extraction_settings.html
    kind_to_fc_parameters: Dict[str, Dict[str, Optional[List[Dict[str, Any]]]]]
        Feature extraction setting for each features.
        See https://tsfresh.readthedocs.io/en/latest/text/feature_extraction_settings.html

    Returns
    ----------
    Converted DataFrame: pd.DataFrame
    """
    if 0 < derivation_window_from:
        raise RuntimeError("derivation_window_from must be less equal than 0.")
    if 0 < derivation_window_to:
        raise RuntimeError("derivation_window_to must be less equal than 0.")
    if derivation_window_to < derivation_window_from:
        raise RuntimeError(
            "derivation_window_from must be less than derivation_window_to."
        )
    if derivation_window_min_size < 0:
        raise RuntimeError("derivation_window_min_size must be greater equal than 0.")

    derivation_window_size = derivation_window_to - derivation_window_from

    if derivation_window_size < derivation_window_min_size:
        raise RuntimeError(
            "derivation_window_min_size must be less equal than derivation_window_size."
        )

    df_rolled = (
        roll_time_series(
            df,
            column_id=group_col,
            column_sort=time_col,
            max_timeshift=derivation_window_size,
            min_timeshift=derivation_window_min_size,
        )
        .drop(columns=[group_col])
        .rename(columns={"id": group_col})
    )

    df_features = (
        extract_features(
            df_rolled,
            column_id=group_col,
            column_sort=time_col,
            default_fc_parameters=default_fc_parameters,
            kind_to_fc_parameters=kind_to_fc_parameters,
        )
        .reset_index()
        .rename(columns={"level_0": group_col, "level_1": time_col})
    )

    if derivation_window_to == 0:
        return df_features

    def _apply_offset(df_g: pd.DataFrame) -> pd.DataFrame:
        t = df_g[time_col].reset_index(drop=True)
        df_offset = (
            df_g.drop(columns=[time_col])
            .shift(-derivation_window_to)
            .reset_index(drop=True)
        )
        return pd.concat([t, df_offset], axis=1).fillna(
            {group_col: df_g[group_col].iloc[0]}
        )

    return df_features.groupby(by=group_col).apply(_apply_offset).reset_index(drop=True)


def derive_features_for_single_series(
    df: pd.DataFrame,
    time_col: str,
    derivation_window_from: int,
    derivation_window_to: int,
    *,
    derivation_window_min_size: int = 0,
    default_fc_parameters: Dict[
        str, Optional[List[Dict[str, Any]]]
    ] = FEATURE_CALC_TYPES[DEFAULT_FEATURE_CALC_TYPE],
    kind_to_fc_parameters: Dict[str, Dict[str, Optional[List[Dict[str, Any]]]]] = {},
    logger: Logger = DEFAULT_LOGGER,
) -> pd.DataFrame:
    """
    Derive features for single time series dataset

    Parameters
    ----------
    df: pd.DataFrame
        Input DataFrame
    time_col: str
        Time column name
    derivation_window_from: int
        Start point of the derivation window. This must be negative value.
    derivation_window_to: int
        End point of the derivation window. This must be negative value.

    Optional Parameters
    -------------------
    derivation_window_min_size: int
        Minimum length of derivation window.
    default_fc_parameters: Dict[str, Optional[List[Dict[str, Any]]]]
        Feature extraction setting.
        See https://tsfresh.readthedocs.io/en/latest/text/feature_extraction_settings.html
    kind_to_fc_parameters: Dict[str, Dict[str, Optional[List[Dict[str, Any]]]]]
        Feature extraction setting for each features.
        See https://tsfresh.readthedocs.io/en/latest/text/feature_extraction_settings.html

    Returns
    ----------
    Converted DataFrame: pd.DataFrame
    """

    return derive_features_for_multi_series(
        add_dummy_group_id(df),
        time_col,
        DEFAULT_GROUP_COL_NAME,
        derivation_window_from,
        derivation_window_to,
        derivation_window_min_size=derivation_window_min_size,
        default_fc_parameters=default_fc_parameters,
        kind_to_fc_parameters=kind_to_fc_parameters,
    ).drop(columns=[DEFAULT_GROUP_COL_NAME])


def derive_features(
    df: pd.DataFrame,
    time_col: str,
    derivation_window_from: int,
    derivation_window_to: int,
    *,
    group_col: Optional[str] = None,
    derivation_window_min_size: int = 0,
    default_fc_parameters: Dict[
        str, Optional[List[Dict[str, Any]]]
    ] = FEATURE_CALC_TYPES[DEFAULT_FEATURE_CALC_TYPE],
    kind_to_fc_parameters: Dict[str, Dict[str, Optional[List[Dict[str, Any]]]]] = {},
    logger: Logger = DEFAULT_LOGGER,
) -> pd.DataFrame:
    """
    Derive features time series dataset

    Parameters
    ----------
    df: pd.DataFrame
        Input DataFrame
    time_col: str
        Time column name
    derivation_window_from: int
        Start point of the derivation window. This must be negative value.
    derivation_window_to: int
        End point of the derivation window. This must be negative value.

    Optional Parameters
    -------------------
    group_col: Optional[str]
        Group ID column name. If None specifed, input DataFrame must be single time series.
    derivation_window_min_size: int
        Minimum length of derivation window.
    default_fc_parameters: Dict[str, Optional[List[Dict[str, Any]]]]
        Feature extraction setting.
        See https://tsfresh.readthedocs.io/en/latest/text/feature_extraction_settings.html
    kind_to_fc_parameters: Dict[str, Dict[str, Optional[List[Dict[str, Any]]]]]
        Feature extraction setting for each features.
        See https://tsfresh.readthedocs.io/en/latest/text/feature_extraction_settings.html

    Returns
    ----------
    Converted DataFrame: pd.DataFrame
    """
    derivation_cols = [col for col in df.columns if col not in [group_col, time_col]]

    if not derivation_cols:
        logger.warning("No features are derived.")
        return df

    if group_col is None:
        return derive_features_for_single_series(
            df,
            time_col,
            derivation_window_from,
            derivation_window_to,
            derivation_window_min_size=derivation_window_min_size,
            default_fc_parameters=default_fc_parameters,
            kind_to_fc_parameters=kind_to_fc_parameters,
        )

    return derive_features_for_multi_series(
        df,
        time_col,
        str(group_col),
        derivation_window_from,
        derivation_window_to,
        derivation_window_min_size=derivation_window_min_size,
        default_fc_parameters=default_fc_parameters,
        kind_to_fc_parameters=kind_to_fc_parameters,
    )


def make_targets_for_multi_series(
    df: pd.DataFrame,
    time_col: str,
    group_col: str,
    target_col: str,
    forecast_window_from: int,
    forecast_window_to: int,
    *,
    known_in_advance_features: List[str] = [],
    distance_col_name: str = DEFAULT_FORECAST_DISTANCE_COL,
    rename_target: Callable[[str], str] = to_forecast_col_name,
    logger: Logger = DEFAULT_LOGGER,
) -> pd.DataFrame:
    """
    Make forecasting targets and related features for multi time series dataset.

    Parameters
    ----------
    df: pd.DataFrame
        Input DataFrame
    time_col: str
        Time column name
    group_col:
        Group ID column name. If None specifed, input DataFrame must be single time series.
    target_col: str
        Target column name
    forecast_window_from: int
        Start point of the forecast window. This must be positive value.
    forecast_window_to: int
        End point of the forecast window. This must be positive value.

    Optional Parameters
    -------------------
    known_in_advance_features: List[str]
        Feature names of known in advance.
    distance_col_name: str
        Forecast distance column name. This column is generated from this function.
    rename_target: Callable[[str], str]
        Renaming function for target variable

    Returns
    ----------
    Generated DataFrame: pd.DataFrame
    """

    def _make_targets(df_g: pd.DataFrame) -> pd.DataFrame:
        return pd.concat(
            [
                pd.concat(
                    [
                        df_g[time_col],
                        df_g[target_col].shift(-distance),
                        pd.Series(
                            np.repeat(distance, len(df_g)), name=distance_col_name
                        ),
                        df_g[time_col].shift(-distance).rename(rename_target(time_col)),
                        df_g[known_in_advance_features].shift(-distance)
                        if known_in_advance_features
                        else None,
                    ],
                    axis=1,
                )
                for distance in range(forecast_window_from, forecast_window_to + 1)
            ]
        )

    return (
        df.groupby(by=group_col)
        .apply(
            lambda df_g: _make_targets(
                df_g.sort_values(by=time_col).reset_index(drop=True)
            )
        )
        .reset_index()
        .rename(columns={"level_0": group_col})
        .drop(columns=["level_1"])
    )


def make_targets_for_single_series(
    df: pd.DataFrame,
    time_col: str,
    target_col: str,
    forecast_window_from: int,
    forecast_window_to: int,
    *,
    distance_col_name: str = DEFAULT_FORECAST_DISTANCE_COL,
    known_in_advance_features: List[str] = [],
    logger: Logger = DEFAULT_LOGGER,
) -> pd.DataFrame:
    """
    Make forecasting targets and related features for single time series dataset.

    Parameters
    ----------
    df: pd.DataFrame
        Input DataFrame
    time_col: str
        Time column name
    target_col: str
        Target column name
    forecast_window_from: int
        Start point of the forecast window. This must be positive value.
    forecast_window_to: int
        End point of the forecast window. This must be positive value.

    Optional Parameters
    -------------------
    distance_col_name: str
        Forecast distance column name. This column is generated from this function.
    known_in_advance_features: List[str]
        Feature names of known in advance.

    Returns
    ----------
    Generated DataFrame: pd.DataFrame
    """
    return make_targets_for_multi_series(
        add_dummy_group_id(df),
        time_col,
        DEFAULT_GROUP_COL_NAME,
        target_col,
        forecast_window_from,
        forecast_window_to,
        distance_col_name=distance_col_name,
        known_in_advance_features=known_in_advance_features,
        logger=logger,
    ).drop(columns=[DEFAULT_GROUP_COL_NAME])


def make_targets(
    df: pd.DataFrame,
    time_col: str,
    target_col: str,
    forecast_window_from: int,
    forecast_window_to: int,
    *,
    group_col: Optional[str] = None,
    distance_col_name: str = DEFAULT_FORECAST_DISTANCE_COL,
    known_in_advance_features: List[str] = [],
    logger: Logger = DEFAULT_LOGGER,
) -> pd.DataFrame:
    """
    Make forecasting targets and related features.

    Parameters
    ----------
    df: pd.DataFrame
        Input DataFrame
    time_col: str
        Time column name
    target_col: str
        Target column name
    forecast_window_from: int
        Start point of the forecast window. This must be positive value.
    forecast_window_to: int
        End point of the forecast window. This must be positive value.

    Optional Parameters
    -------------------
    group_col: Optional[str]
        Group ID column name. If None specifed, input DataFrame must be single time series.
    distance_col_name: str
        Forecast distance column name. This column is generated from this function.
    known_in_advance_features: List[str]
        Feature names of known in advance.

    Returns
    ----------
    Generated DataFrame: pd.DataFrame
    """
    if group_col is None:
        return make_targets_for_single_series(
            df,
            time_col,
            target_col,
            forecast_window_from,
            forecast_window_to,
            distance_col_name=distance_col_name,
            known_in_advance_features=known_in_advance_features,
            logger=logger,
        )

    return make_targets_for_multi_series(
        df,
        time_col,
        str(group_col),
        target_col,
        forecast_window_from,
        forecast_window_to,
        distance_col_name=distance_col_name,
        known_in_advance_features=known_in_advance_features,
        logger=logger,
    )


def make_derivation_ignored_features(
    df: pd.DataFrame,
    ignore_derivation: List[str],
    time_col: str,
    group_col: Optional[str] = None,
    known_in_advance_features: List[str] = [],
) -> Optional[pd.DataFrame]:
    cols = [
        col
        for col in [group_col, time_col, *ignore_derivation]
        if (col is not None and col not in known_in_advance_features)
    ]

    if not cols:
        return None

    return df[cols].rename(
        columns={col: to_no_derivation_col_name(col) for col in ignore_derivation}
    )


def make_timeseries_modeling_dataset(
    df: pd.DataFrame,
    time_col: str,
    target_col: str,
    derivation_window_from: int,
    derivation_window_to: int,
    forecast_window_from: int,
    forecast_window_to: int,
    *,
    group_col: Optional[str] = None,
    derivation_window_min_size: int = 0,
    default_fc_parameters: Dict[
        str, Optional[List[Dict[str, Any]]]
    ] = FEATURE_CALC_TYPES[DEFAULT_FEATURE_CALC_TYPE],
    kind_to_fc_parameters: Dict[str, Dict[str, Optional[List[Dict[str, Any]]]]] = {},
    distance_col_name: str = DEFAULT_FORECAST_DISTANCE_COL,
    known_in_advance_features: List[str] = [],
    ignore_derivation: List[str] = [],
    prediction_point: Optional[pd.Timestamp] = None,
    logger: Logger = DEFAULT_LOGGER,
) -> pd.DataFrame:
    """
    Convert to time series dataset

    Parameters
    ----------
    df: pd.DataFrame
        Input DataFrame
    time_col: str
        Time column name
    target_col: str
        Target column name
    derivation_window_from: int
        Start point of the derivation window. This must be negative value.
    derivation_window_to: int
        End point of the derivation window. This must be negative value.
    forecast_window_from: int
        Start point of the forecast window. This must be positive value.
    forecast_window_to: int
        End point of the forecast window. This must be positive value.

    Optional Parameters
    -------------------
    group_col: Optional[str]
        Group ID column name. If None specifed, input DataFrame must be single time series.
    derivation_window_min_size: int
        Minimum length of derivation window.
    default_fc_parameters: Dict[str, Optional[List[Dict[str, Any]]]]
        Feature extraction setting.
        See https://tsfresh.readthedocs.io/en/latest/text/feature_extraction_settings.html
    kind_to_fc_parameters: Dict[str, Dict[str, Optional[List[Dict[str, Any]]]]]
        Feature extraction setting for each features.
        See https://tsfresh.readthedocs.io/en/latest/text/feature_extraction_settings.html
    distance_col_name: str
        Forecast distance column name. This column is generated from this function.
    known_in_advance_features: List[str]
        Feature names of known in advance.
    ignore_derivation: List[str]
        Feature names not to be derived
    prediction_point: Optional[pd.Timestamp]
        The time stamp of prediction point.
        The feature derivations are applied to the dataset before this timestamp.
        The conversion result dataset after this prediction point will be removed.

    Returns
    ----------
    Converted DataFrame: pd.DataFrame
    """

    # Cut forecasting range
    df_derivation_input = (
        df if prediction_point is None else df[df[time_col] <= prediction_point]
    )

    logger.debug("Now deriving features...")
    df_features = derive_features(
        df_derivation_input.drop(
            columns=[*known_in_advance_features, *ignore_derivation]
        ),
        time_col,
        derivation_window_from,
        derivation_window_to,
        group_col=group_col,
        derivation_window_min_size=derivation_window_min_size,
        default_fc_parameters=default_fc_parameters,
        kind_to_fc_parameters=kind_to_fc_parameters,
        logger=logger,
    )

    logger.debug("Now making targets and known in advance features...")
    df_targets = make_targets(
        df,
        time_col,
        target_col,
        forecast_window_from,
        forecast_window_to,
        group_col=group_col,
        distance_col_name=distance_col_name,
        known_in_advance_features=known_in_advance_features,
        logger=logger,
    )

    logger.debug("Now retrieving derivation ignored features...")
    df_derivation_ignored = make_derivation_ignored_features(
        df_derivation_input,
        ignore_derivation,
        time_col,
        group_col=group_col,
        known_in_advance_features=known_in_advance_features,
    )

    logger.debug("Now finalizing...")
    df_targets = (
        df_targets
        if prediction_point is None
        else df_targets[df_targets[time_col] == prediction_point]
    )

    join_key = [col for col in [group_col, time_col] if col is not None]

    df_ret = df_targets

    # Join derivation ignored features
    df_ret = (
        pd.merge(df_ret, df_derivation_ignored, how="left", on=join_key,)
        if df_derivation_ignored is not None
        else df_ret
    )

    # Join derived features
    df_ret = pd.merge(df_ret, df_features, how="left", on=join_key)

    return df_ret.infer_objects()


def create_root_logger(filepath: str, level: str, quiet: bool) -> Logger:
    import logging
    from logging import StreamHandler, Formatter

    logger: Logger = getLogger()
    logger.setLevel(level)

    if quiet:
        return logger

    default_format = "%(asctime)s - %(name)s - %(levelname)s - %(message)s"
    formatter = logging.Formatter(default_format)

    def add_handler(handler: StreamHandler, fmt: Formatter) -> None:
        handler.setFormatter(fmt)
        handler.setLevel(level)
        logger.addHandler(handler)

    add_handler(StreamHandler(), formatter)

    if filepath:
        add_handler(
            logging.FileHandler(filepath, mode="w", encoding="utf-8"), formatter
        )

    return logger


def parse_args(args: List[str]) -> Namespace:
    parser = ArgumentParser()

    # Common settings
    parser.add_argument(
        "input", action="store", type=str, help="Input file path",
    )
    parser.add_argument(
        "output", action="store", type=str, help="Output file path",
    )
    parser.add_argument(
        "time_col", action="store", type=str, help="Time column name",
    )
    parser.add_argument(
        "target_col", action="store", type=str, help="Target column name",
    )
    parser.add_argument(
        "-g",
        "--group-col",
        action="store",
        type=str,
        default="ID",
        help="Group ID for multi time series.",
    )
    parser.add_argument(
        "-d",
        "--derivation-window",
        action="store",
        type=str,
        nargs=2,
        default=[1, 0],
        help="Feature derivation window range. e.g.) '-d 5 3' means derivation from '-5' to '-3'",
    )
    parser.add_argument(
        "-f",
        "--forecast-window",
        action="store",
        type=str,
        nargs=2,
        default=[1, 0],
        help="Forecast window range. e.g.) '-f 1 7' means forecast from '1' to '7'",
    )
    parser.add_argument(
        "-k",
        "--known-in-advance",
        action="store",
        type=str,
        nargs="*",
        default=[],
        help="Known in advance features",
    )
    parser.add_argument(
        "-i",
        "--ignore-derivation",
        action="store",
        type=str,
        nargs="*",
        default=[],
        help="Feature names for ignore derivation",
    )
    parser.add_argument(
        "-c",
        "--calc",
        action="store",
        type=str,
        choices=list(FEATURE_CALC_TYPES.keys()),
        default=DEFAULT_FEATURE_CALC_TYPE,
        help="Calculation feature type. See https://tsfresh.readthedocs.io/en/latest/text/feature_extraction_settings.html",
    )
    parser.add_argument(
        "-k",
        "--kind-to-fc",
        action="store",
        type=str,
        default=None,
        help="The JSON file path of the calculation feature type for the specified features."
        + " See https://tsfresh.readthedocs.io/en/latest/text/feature_extraction_settings.html",
    )
    parser.add_argument(
        "-e",
        "--encoding",
        action="store",
        type=str,
        default="utf-8-sig",
        help="Input/Output file encoding",
    )
    parser.add_argument(
        "-v", "--version", action="version", version=f"%(prog)s {__version__}"
    )

    # Logging settings
    parser.add_argument(
        "-l", "--log", action="store", type=str, default=None, help="Log file path."
    )
    parser.add_argument(
        "-L",
        "--log-level",
        action="store",
        type=str,
        choices=["DEBUG", "INFO", "WARN", "ERROR", "FATAL"],
        default="DEBUG",
        help="Log level.",
    )
    parser.add_argument(
        "-q", "--quiet", dest="quiet", action="store_true", help="Quiet output."
    )
    parser.set_defaults(quiet=False)
    parser.add_argument(
        "-V",
        "--verbose",
        dest="verbose",
        action="store_true",
        help="Enable verbose output.",
    )
    parser.set_defaults(verbose=False)

    return parser.parse_args(args)


def load_kind_to_fc_parameters(
    filepath: str,
) -> Dict[str, Dict[str, Optional[List[Dict[str, Any]]]]]:
    import json

    with open(filepath) as f:
        return json.load(f)


if __name__ == "__main__":
    args = parse_args(sys.argv[1:])

    logger = create_root_logger(
        filepath=args.log, level=args.log_level, quiet=args.quiet
    )

    logger.info(f"{__file__} v{__version__}")

    logger.info("Now loading input file...")
    df_input = pd.read_csv(args.input, encoding=args.encoding)

    logger.info("Now converting...")
    df_output = make_timeseries_modeling_dataset(
        df_input,
        args.time_col,
        args.target_col,
        args.derivation_window[0],
        args.derivation_window[1],
        args.forecast_window[0],
        args.forecast_window[1],
        group_col=args.group_col,
        known_in_advance_features=args.known_in_advance,
        ignore_derivation=args.ignore_derivation,
        default_fc_parameters=FEATURE_CALC_TYPES[args.calc],
        kind_to_fc_parameters=load_kind_to_fc_parameters(args.kind_to_fc)
        if args.kind_to_fc
        else {},
        logger=logger,
    )

    logger.info("Now writing input file...")
    df_output.to_csv(args.output, encoding=args.encoding, index=False)

    logger.info("Successfully finished!")
